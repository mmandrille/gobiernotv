from __future__ import unicode_literals
from django.db import models

#Import Modulos Extra
from tinymce.models import HTMLField

#Import Personales
from .api import obtener_organismos
from .validators import validate_file_extension

#Create your models here.
class Funcionario(models.Model):
    apellidos = models.CharField('Apellidos', max_length=100)
    nombres = models.CharField('Nombres', max_length=100)
    organismo = models.PositiveIntegerField(choices= obtener_organismos(), default=0)
    num_doc = models.CharField('Numero de Documento', max_length=50, null=True, blank=True)
    telefono = models.CharField('Telefono', max_length=20, null=True, blank=True)
    email = models.EmailField('Correo Electronico Personal', null=True, blank=True)
    password = models.CharField(max_length=25)
    foto = models.FileField('Foto de Perfil', upload_to='retratos/', null=True, blank=True)
    max_time = models.PositiveIntegerField(default=45)
    Activo = models.BooleanField('Activo', default=True)
    def __str__(self):
        return self.apellidos + " " + self.nombres
    def nombre_organismo(self):
        organismos = obtener_organismos()
        print(organismos)
        return True

class Video(models.Model):
    funcionario = models.ForeignKey(Funcionario, on_delete=models.CASCADE, related_name="videos")
    titulo = models.CharField('Titulo', max_length=100)
    descripcion = HTMLField(blank=True)
    archivo = models.FileField(upload_to='videos/', validators=[validate_file_extension])
    fecha = models.DateTimeField(auto_now_add=True)
    aprobado = models.BooleanField('Aprobado', default=False)
    def __str__(self):
        return self.titulo + " de " + self.funcionario.apellidos + " " + self.funcionario.nombres

class Intro(models.Model):
    organismo = models.PositiveIntegerField(choices= obtener_organismos(), null=True, blank=True)
    titulo = models.CharField('Titulo', max_length=100)
    archivo = models.FileField(upload_to='intros/', validators=[validate_file_extension])
    fecha = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.titulo + " de " + self.get_organismo_display()

class Outro(models.Model):
    organismo = models.PositiveIntegerField(choices= obtener_organismos(), null=True, blank=True)
    titulo = models.CharField('Titulo', max_length=100)
    archivo = models.FileField(upload_to='outros/', validators=[validate_file_extension])
    fecha = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.titulo + " de " + self.get_organismo_display()