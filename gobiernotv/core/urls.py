from django.conf.urls import url
from django.urls import path
from django.views.decorators.csrf import csrf_exempt

#Import personales
from . import views

app_name = 'core'
urlpatterns = [
    #Basicas:
    url(r'^$', views.home, name='home'),
    
    #App urls
    path('app_login', csrf_exempt(views.app_login), name='app_login'),
    path('upload_video', csrf_exempt(views.upload_video), name='upload_video'),
]