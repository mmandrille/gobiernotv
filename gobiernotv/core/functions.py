#Import Standard
from django.core.exceptions import ObjectDoesNotExist

#Import Personales
from core.api import obtener_full_organismos

#Funciones reutilizables
def obtener_recurso(modelo, org_id):
    try:
        return modelo.objects.get(organismo=org_id).archivo.url
    except ObjectDoesNotExist:
        return obtener_recurso(modelo, obtener_full_organismos()[org_id][1])