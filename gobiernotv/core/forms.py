#Imports Standar
from django import forms
from django.core.exceptions import ValidationError

#Import Personales
from core.models import Video

#Definimos los Forms
class VideoForm(forms.ModelForm):
    email = forms.EmailField(required=True)
    password = forms.CharField(widget=forms.PasswordInput)
    
    class Meta:
        model= Video
        fields= ["funcionario", "titulo", "descripcion", "archivo"]
    
    def clean(self):
        form_data = self.cleaned_data
        print(form_data)
        f = form_data['funcionario']
        if form_data.get('email', None) != f.email:
            raise ValidationError("Email Incorrecto!")
        if form_data.get('password', None) != f.password:
            raise ValidationError("Password Incorrecta!")
        return form_data