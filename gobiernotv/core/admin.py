from django.contrib import admin
#Modulos para no permitir escalacion de privilegios
from django.contrib.auth.admin import UserAdmin, User
#Incluimos modelos
from .models import Funcionario, Video
from .models import Intro, Outro
#Modificacion del panel de administrador para no permitir escalacion de privilegios
class RestrictedUserAdmin(UserAdmin):
    model = User
    def formfield_for_dbfield(self, db_field, **kwargs):
        field = super(RestrictedUserAdmin, self).formfield_for_dbfield(db_field, **kwargs)
        user = kwargs['request'].user
        if not user.is_superuser:
            if db_field.name == 'groups':
                field.queryset = field.queryset.filter(id__in=[i.id for i in user.groups.all()])
            if db_field.name == 'user_permissions':
                field.queryset = field.queryset.filter(id__in=[i.id for i in user.user_permissions.all()])
            if db_field.name == 'is_superuser':
                field.widget.attrs['disabled'] = True
        return field

#Realizamos modificaciones sobre los modelos
class FuncionarioAdmin(admin.ModelAdmin):
    search_fields = ['nombres', 'apellidos']
    list_filter = ['organismo']
    #autocomplete_fields = ("organismo",)

class VideoAdmin(admin.ModelAdmin):
    search_fields = ['funcionario__nombres', 'funcionario__apellidos', 'titulo']
    list_filter = ['aprobado', 'funcionario__organismo', ]

class IntroAdmin(admin.ModelAdmin):
    list_filter = ['organismo']

class OutroAdmin(admin.ModelAdmin):
    list_filter = ['organismo']

#Registramos modificaciones para no permitir escalacion de privilegios
admin.site.unregister(User)
admin.site.register(User, RestrictedUserAdmin)
# Register your models here.
admin.site.register(Funcionario, FuncionarioAdmin)
admin.site.register(Video, VideoAdmin)
admin.site.register(Intro, IntroAdmin)
admin.site.register(Outro, OutroAdmin)