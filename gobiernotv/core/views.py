import json
import requests 
from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from django.utils.html import escape
from django.views.decorators.http import require_http_methods

#Import Personales
from .models import Funcionario, Video, Intro, Outro
from .functions import obtener_recurso
from .forms import VideoForm

# Create your views here.
def home(request):
    last_video = Video.objects.filter(aprobado=True).order_by('fecha').first()
    videos = Video.objects.filter(aprobado=True).exclude(pk=last_video.pk).order_by('fecha')[:6]
    return render(request, 'home.html', {'last_video': last_video, 'videos': videos, })

@require_http_methods(["POST"])
def app_login(request):
    r = json.loads(request.body.decode("utf-8"))
    if r["action"] == "login_action":
        f = Funcionario.objects.filter(email= r["login_email"], password= r["login_password"]).first()
        if f:
            return JsonResponse(
                {"action":"login_action",
                "success": True,
                "organismo": f.get_organismo_display(),
                "funcionario": f.pk,
                "apellidos": f.apellidos,
                "nombres": f.nombres,
                "intro_url": 'http://'+request.META['HTTP_HOST']+'/'+obtener_recurso(Intro, f.organismo),
                "outro_url": 'http://'+request.META['HTTP_HOST']+'/'+obtener_recurso(Outro, f.organismo),
                },safe=False)
        else:
            return JsonResponse(
                {"action":"login_action",
                "success": False,
                },safe=False)


def upload_video(request):
    form= VideoForm(request.POST or None, request.FILES or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return JsonResponse(
                    {
                    "action":"upload_video",
                    "success": True,
                    },safe=False)
        else:
            return JsonResponse(
                    {
                    "action":"upload_video",
                    "success": False,
                    "errores": str(form.errors.as_data()),
                    },safe=False)
    else:
        return render(request, 'videos.html', {'form': form, })