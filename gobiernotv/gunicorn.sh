#!/bin/bash
cd /opt/gobiernotv
source venv/bin/activate
cd /opt/gobiernotv/gobiernotv
gunicorn gobiernotv.wsgi -t 600 -b 127.0.0.1:8017 -w 6 --user=servidor --group=servidor --log-file=/opt/gobiernotv/gunicorn.log 2>>/opt/gobiernotv/gunicorn.log
